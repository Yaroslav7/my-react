import * as React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import App from './App';

export const AppRouter: React.StatelessComponent<{}> = () => {
  return (
    <HashRouter>
      <div className="container-fluid">
        <Route path="/" />
        <Switch>
          <Route exact="true" path="/" component={App} />
          <Route path="/about" />
          <Route path="/members" component={App} />
        </Switch>
      </div>
    </HashRouter>
  );
}